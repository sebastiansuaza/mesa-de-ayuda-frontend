import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit
} from '@angular/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { MediaMatcher } from '@angular/cdk/layout';
import { MenuItems } from '../../../shared/menu-items/menu-items';

import { Usuario } from '../../../shared/models/usuario.model';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: []
})
export class AppSidebarComponent implements OnDestroy {
    public config: PerfectScrollbarConfigInterface = {};
    mobileQuery: MediaQueryList;
    usuario: Usuario;

    private _mobileQueryListener: () => void;
    status: boolean = false;
    clickEvent() {
      this.status = !this.status;
    }

    subclickEvent() {
      this.status = true;
    }
    constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, public menuItems: MenuItems) {
      this.mobileQuery = media.matchMedia('(min-width: 768px)');
      this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);

      this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
    }

    contaisProfiles(perfiles_: string[]) {
      if (perfiles_.length == 0) {
        return true;
      } else {
        for (var i = 0; i < perfiles_.length; ++i) {
          for (var j = 0; j < this.usuario.perfiles.length; ++j) {
            if (this.usuario.perfiles[j].perfil.perfil === perfiles_[i]) {
              return true;
            }
          }
        }
        return false;
      }
    }

    ngOnDestroy(): void {
      this.mobileQuery.removeListener(this._mobileQueryListener);
    }
}
