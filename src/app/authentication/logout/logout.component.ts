import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { OAuthService } from 'angular-oauth2-oidc';

import { Constantes } from '../../shared/models/constantes.model';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

	constructor(private oauthService: OAuthService, private router: Router, private http: HttpClient) {

	}

  	ngOnInit() {
		this.oauthService.logOut();
  	}
}
