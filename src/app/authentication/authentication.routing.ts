import { Routes } from '@angular/router';

import { ErrorComponent } from './error/error.component';
import { CallbackComponent } from './callback/callback.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';

export const AuthenticationRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '404',
        component: ErrorComponent
      }, {
        path: 'callback',
        component: CallbackComponent
      }, {
        path: 'login',
        component: LoginComponent
      }, {
        path: 'logout',
        component: LogoutComponent
      }
    ]
  }
];
