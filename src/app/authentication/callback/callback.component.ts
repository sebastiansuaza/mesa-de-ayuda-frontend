import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { OAuthService, OAuthEvent } from 'angular-oauth2-oidc';

import { UsuarioService } from '../../shared/services/usuario.service';

@Component({
  	selector: 'app-callback',
	templateUrl: './callback.component.html',
	styleUrls: ['./callback.component.scss']
})
export class CallbackComponent implements OnInit {
	constructor(private oauthService: OAuthService, private usuarioService: UsuarioService, private router: Router) {

  	}

	ngOnInit() {
		if (this.oauthService.hasValidAccessToken()) {
			this.validarUsuario();
			this.router.navigate(['']);
		} else {
			this.oauthService.tryLogin({
				onTokenReceived: context => {
					if (this.oauthService.hasValidAccessToken()) {
						this.oauthService.setupAutomaticSilentRefresh();
						this.validarUsuario();
						this.router.navigate(['']);
					} else {
						this.router.navigate(['/authentication/login']);
					}
				}
			});
		}
  	}

  	validarUsuario() {
		this.usuarioService.user().subscribe(data => {
			sessionStorage.setItem('usuario', JSON.stringify(data));
			this.router.navigate(['']);
		});
  	}
}
