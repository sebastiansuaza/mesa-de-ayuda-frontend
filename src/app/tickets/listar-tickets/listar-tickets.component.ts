import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';

import { Usuario } from '../../shared/models/usuario.model';
import { Ticket } from '../../shared/models/ticket.model';
import { Pagina } from '../../shared/models/paged/pagina.model';

import { TicketService } from '../../shared/services/ticket.service';

@Component({
  selector: 'app-listar-tickets',
  templateUrl: './listar-tickets.component.html',
  styleUrls: ['./listar-tickets.component.css']
})
export class ListarTicketsComponent implements OnInit {
	usuario: Usuario;
	pagina = new Pagina();
	loading = true;
	rows: Ticket[];
	sort: any[];

	@ViewChild(ListarTicketsComponent) table: ListarTicketsComponent;
	constructor(private ticketService: TicketService) {
		this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
	}

	ngOnInit() {
		this.sort = [{ prop: "codigo", dir: "asc" }];
		this.setPage({ offset: 0 });
	}

	obtenerRegistros() {
		this.ticketService.obtenerPaginaPorOrganizacion(this.pagina.number, this.sort).subscribe(data => {
			this.pagina = data;
			this.rows = data.content;
			this.loading = false;
		});
	}

	setPage(pageInfo) {
		this.loading = true;
		this.pagina.number = pageInfo.offset;
		this.obtenerRegistros();
	}

	onSort(event) {
		var pagina = 0;
		this.sort[0].prop = event.sorts[0].prop;
		this.sort[0].dir = event.sorts[0].dir;

		this.ticketService.obtenerPaginaPorOrganizacion(pagina, this.sort).subscribe(data => {
			this.pagina = data;
			this.rows = data.content;
		});
	}
}
