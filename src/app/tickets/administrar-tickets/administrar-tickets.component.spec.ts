import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrarTicketsComponent } from './administrar-tickets.component';

describe('AdministrarTicketsComponent', () => {
  let component: AdministrarTicketsComponent;
  let fixture: ComponentFixture<AdministrarTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrarTicketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrarTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
