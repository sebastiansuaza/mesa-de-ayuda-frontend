import 'hammerjs';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { TicketsRoutes } from './tickets.routing';

import { AdministrarTicketsComponent } from './administrar-tickets/administrar-tickets.component';
import { AgregarTicketsComponent } from './agregar-tickets/agregar-tickets.component';
import { SeguimientoTicketsComponent } from './seguimiento-tickets/seguimiento-tickets.component';
import { CambioEstadoTicketsComponent } from './cambio-estado-tickets/cambio-estado-tickets.component';
import { ListarTicketsComponent } from './listar-tickets/listar-tickets.component';

@NgModule({
	imports: [
		CommonModule,
		DemoMaterialModule,
		FormsModule,
		ReactiveFormsModule,
		FlexLayoutModule,
		NgxDatatableModule,
		RouterModule.forChild(TicketsRoutes)
	],
  	declarations: [AdministrarTicketsComponent, AgregarTicketsComponent, SeguimientoTicketsComponent, CambioEstadoTicketsComponent, ListarTicketsComponent]
})
export class TicketsModule { }
