import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CambioEstadoTicketsComponent } from './cambio-estado-tickets.component';

describe('CambioEstadoTicketsComponent', () => {
  let component: CambioEstadoTicketsComponent;
  let fixture: ComponentFixture<CambioEstadoTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CambioEstadoTicketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CambioEstadoTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
