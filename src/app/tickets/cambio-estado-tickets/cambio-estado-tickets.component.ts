import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { Usuario } from '../../shared/models/usuario.model';
import { Estado } from '../../shared/models/estado.model';
import { TicketEstado } from '../../shared/models/ticketEstado.model';
import { Ticket } from '../../shared/models/ticket.model';

import { EstadoService } from '../../shared/services/estado.service';
import { TicketService } from '../../shared/services/ticket.service';

@Component({
  	selector: 'app-cambio-estado-tickets',
  	templateUrl: './cambio-estado-tickets.component.html',
  	styleUrls: ['./cambio-estado-tickets.component.css']
})
export class CambioEstadoTicketsComponent implements OnInit {
	submitted = false;
	codigo: string;
	usuario: Usuario;
	ticket: Ticket;
	ticketEstado: TicketEstado;
	estados: Estado[];

	constructor(private location: Location, private route: ActivatedRoute, 
		private router: Router, private estadoService: EstadoService, private ticketService: TicketService) {
		this.ticket = new Ticket();

		this.usuario = JSON.parse(sessionStorage.getItem('usuario'));

		this.ticketEstado = new TicketEstado();
		this.ticketEstado.usuario = this.usuario;
		this.ticketEstado.activo = 1;
  	}

  	ngOnInit() {
		this.codigo = this.route.snapshot.params['codigo'];

		if (this.codigo == undefined || this.codigo == null) {
			this.router.navigate(['/ticket']);
		} else {
			this.ticketService.buscarPorId(+this.codigo).subscribe(
				data => {
					this.ticket = data;
					for (var i = 0; i < this.ticket.estados.length; ++i) {
						this.ticket.estados[i].activo = 0;
					}
				}, error => console.log(error)
			);

			this.estadoService.listar().subscribe(
				data => this.estados = data , error => console.log(error)
			);
		}
  	}

	volver() {
		this.location.back();
	}

	onSubmit() {
		this.submitted = true;
		this.ticket.estados.push(this.ticketEstado);
		this.ticketService.modificar(this.ticket).subscribe(
			data => {
				//this.notificacionService.notificar("Agregado con exito");
				this.router.navigate(['/ticket/seguimiento/' + this.ticket.codigo]);
			}, error => {
				this.submitted = false;
				console.log(error)
			}
		)
	}
}
