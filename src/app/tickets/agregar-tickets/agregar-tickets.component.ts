import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { Usuario } from '../../shared/models/usuario.model';
import { Estado } from '../../shared/models/estado.model';
import { Categoria } from '../../shared/models/categoria.model';
import { Organizacion } from '../../shared/models/organizacion.model';
import { TicketEstado } from '../../shared/models/ticketEstado.model';
import { Ticket } from '../../shared/models/ticket.model';

import { CategoriaService } from '../../shared/services/categoria.service';
import { OrganizacionService } from '../../shared/services/organizacion.service';
import { TicketService } from '../../shared/services/ticket.service';

@Component({
  	selector: 'app-agregar-tickets',
  	templateUrl: './agregar-tickets.component.html',
  	styleUrls: ['./agregar-tickets.component.css']
})
export class AgregarTicketsComponent implements OnInit {
	submitted = false;

	usuario: Usuario;
	ticket: Ticket;
	ticketEstado: TicketEstado;
	estado: Estado;
	categorias: Categoria[];
	organizaciones: Organizacion[];

	constructor(private location: Location, private router: Router, private categoriaService: CategoriaService, 
		private organizacionService: OrganizacionService, private ticketService: TicketService) {
		this.ticket = new Ticket();
		this.ticket.estados = [];
		this.ticketEstado = new TicketEstado();

		this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
		this.ticket.organizacion = this.usuario.organizacion.organizacion;
		this.ticket.usuario = this.usuario;

		this.estado = new Estado();
		this.estado.codigo = 1;

		this.ticketEstado.usuario = this.usuario;
		this.ticketEstado.comentario = 'Estado radicado Autogenerado.';
		this.ticketEstado.activo = 1;
		this.ticketEstado.estado = this.estado;

		this.ticket.estados.push(this.ticketEstado);
	}

	ngOnInit() {
		this.categoriaService.listar().subscribe(data => {
				this.categorias = data;
			}, error => console.log(error)
		)

		this.organizacionService.listar().subscribe(data => {
				this.organizaciones = data;
			}, error => console.log(error)
		)
	}

	volver() {
		this.location.back();
	}

	onSubmit() {
		this.submitted = true;
		this.ticketService.agregar(this.ticket).subscribe(
			data => {
				//this.notificacionService.notificar("Agregado con exito");
				this.router.navigate(['/ticket']);
			}, error => {
				this.submitted = false;
				console.log(error)
			}
		)
	}
}
