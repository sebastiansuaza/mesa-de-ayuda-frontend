import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Usuario } from '../../shared/models/usuario.model';
import { Ticket } from '../../shared/models/ticket.model';

import { TicketService } from '../../shared/services/ticket.service';

@Component({
  	selector: 'app-seguimiento-tickets',
  	templateUrl: './seguimiento-tickets.component.html',
  	styleUrls: ['./seguimiento-tickets.component.scss']
})
export class SeguimientoTicketsComponent implements OnInit {
	stacked = false;
	codigo: string;
	ticket: Ticket;

	constructor(private route: ActivatedRoute, private router: Router, private ticketService: TicketService) {
		this.ticket = new Ticket();
  	}

  	ngOnInit() {
		this.codigo = this.route.snapshot.params['codigo'];

		if (this.codigo == undefined || this.codigo == null) {
			this.router.navigate(['/ticket']);
		} else {
			this.ticketService.buscarPorId(+this.codigo).subscribe(
				data => this.ticket = data, error => console.log(error)
			);	
		}
  	}
}
