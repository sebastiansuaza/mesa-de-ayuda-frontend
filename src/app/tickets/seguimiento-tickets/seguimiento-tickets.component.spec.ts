import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguimientoTicketsComponent } from './seguimiento-tickets.component';

describe('SeguimientoTicketsComponent', () => {
  let component: SeguimientoTicketsComponent;
  let fixture: ComponentFixture<SeguimientoTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguimientoTicketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguimientoTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
