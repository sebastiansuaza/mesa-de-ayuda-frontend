import { Routes } from '@angular/router';

import { AdministrarTicketsComponent } from './administrar-tickets/administrar-tickets.component';
import { AgregarTicketsComponent } from './agregar-tickets/agregar-tickets.component';
import { SeguimientoTicketsComponent } from './seguimiento-tickets/seguimiento-tickets.component';
import { CambioEstadoTicketsComponent } from './cambio-estado-tickets/cambio-estado-tickets.component';

export const TicketsRoutes: Routes = [{
	path: '',
	children: [{
    	path: '',
    	component: AdministrarTicketsComponent
	}, {
		path: 'agregar',
		component: AgregarTicketsComponent
	}, {
		path: 'seguimiento/:codigo',
		component: SeguimientoTicketsComponent
	}, {
		path: 'agregar/estado/:codigo',
		component: CambioEstadoTicketsComponent
	}]
}];
