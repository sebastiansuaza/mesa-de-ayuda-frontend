import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { AppBlankComponent } from './layouts/blank/blank.component';

import { AuthGuard } from './shared/auth/guard/auth.guard';

export const AppRoutes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: '',
        redirectTo: '/dashboards',
        pathMatch: 'full'
      }, {
        path: 'dashboards',
        loadChildren: './dashboards/dashboards.module#DashboardsModule',
        canActivate: [AuthGuard]
      }, {
        path: 'usuario',
        loadChildren: './usuarios/usuarios.module#UsuariosModule',
        canActivate: [AuthGuard]
      }, {
        path: 'ticket',
        loadChildren: './tickets/tickets.module#TicketsModule',
        canActivate: [AuthGuard]
      }
    ]
  }, {
    path: '',
    component: AppBlankComponent,
    children: [
      {
        path: 'authentication',
        loadChildren: './authentication/authentication.module#AuthenticationModule'
      }
    ]
  }, {
    path: '**',
    redirectTo: 'authentication/404'
  }
];
