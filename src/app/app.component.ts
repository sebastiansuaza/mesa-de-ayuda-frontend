import { Component } from '@angular/core';
import { OAuthService, NullValidationHandler } from 'angular-oauth2-oidc';

import { authImplicitFlowConfig } from './shared/auth/auth.implicit.config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

	constructor(private oauthService: OAuthService) {
		this.configureWithNewConfigApi();
	}

	private configureWithNewConfigApi() {
		this.oauthService.configure(authImplicitFlowConfig);
		this.oauthService.setStorage(sessionStorage);
		this.oauthService.tokenValidationHandler = new NullValidationHandler();
		//this.oauthService.loadDiscoveryDocument(authImplicitFlowConfig.loginUrl);

		// Load Discovery Document and then try to login the user
		//this.oauthService.loadDiscoveryDocument(authImplicitFlowConfig.loginUrl).then(() => {
		//	this.oauthService.tryLogin();
		//});
		//this.oauthService.initImplicitFlow();
		//this.oauthService.loadDiscoveryDocumentAndTryLogin();
	}
}
