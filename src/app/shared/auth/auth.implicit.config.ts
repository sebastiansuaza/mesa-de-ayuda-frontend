import { AuthConfig } from 'angular-oauth2-oidc';

export const authImplicitFlowConfig: AuthConfig = {
    loginUrl: 'http://localhost:8081/mesa_ayuda_sso/oauth/authorize',
    redirectUri: window.location.origin + '/authentication/callback',
    //userinfoEndpoint: 'http://localhost:8082/mesa_ayuda/api/algo',
    clientId: 'AngularClient',
    scope: 'read write',
    responseType: 'token',
    showDebugInformation: true,
    requireHttps: false,
    logoutUrl: 'http://localhost:8081/mesa_ayuda_sso/api/logout',
    postLogoutRedirectUri: window.location.origin + '/authentication/login',
    silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',
    oidc: false
}