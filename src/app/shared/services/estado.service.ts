import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Constantes } from '../models/constantes.model';
import { Estado } from '../models/estado.model';

@Injectable({
    providedIn: 'root'
})
export class EstadoService {

    private url: string = "";

    constructor(private http: HttpClient) {
        this.url = this.obtenerUrl();
    }

    obtenerUrl() {
        return Constantes.baseUrl + Constantes.apiEstado;
    }

    listar(): Observable<Estado[]> {
        return this.http.get<Estado[]>(this.url)
            .pipe(catchError(this.handleError));
    }
    
    total(): Observable<number> {
        return this.http.get<number>(this.url + '/total')
            .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error('El servidor ha retornado un error');
        }
        return throwError('Something bad happened; please try again later.');
    };
}
