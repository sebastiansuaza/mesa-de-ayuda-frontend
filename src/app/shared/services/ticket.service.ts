import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Constantes } from '../models/constantes.model';
import { Ticket } from '../models/ticket.model';
import { Pagina } from '../models/paged/pagina.model';

@Injectable({
    providedIn: 'root'
})
export class TicketService {

    private url: string = "";

    constructor(private http: HttpClient) {
        this.url = this.obtenerUrl();
    }

    obtenerUrl() {
        return Constantes.baseUrl + Constantes.apiTicket;
    }

    buscarPorId(id: number): Observable<Ticket> {
        return this.http.get<Ticket>(this.url + '/id/ ' + id)
            .pipe(catchError(this.handleError));
    }

    listar(): Observable<Ticket[]> {
        return this.http.get<Ticket[]>(this.url)
            .pipe(catchError(this.handleError));
    }

    total(): Observable<number> {
        return this.http.get<number>(this.url + '/total')
            .pipe(catchError(this.handleError));
    }

    obtenerPagina(pagina: number, sort: any[]): Observable<Pagina<Ticket>> {
        return this.http.get<Pagina<Ticket>>(this.url + '/paginacion?page=' + pagina + '&sort=' + sort[0].prop + ',' + sort[0].dir)
            .pipe(catchError(this.handleError));
    }

    obtenerPaginaPorUsuario(pagina: number, sort: any[]): Observable<Pagina<Ticket>> {
        return this.http.get<Pagina<Ticket>>(this.url + '/usuario/paginacion?page=' + pagina + '&sort=' + sort[0].prop + ',' + sort[0].dir)
            .pipe(catchError(this.handleError));
    }

    obtenerPaginaPorOrganizacion(pagina: number, sort: any[]): Observable<Pagina<Ticket>> {
        return this.http.get<Pagina<Ticket>>(this.url + '/organizacion/paginacion?page=' + pagina + '&sort=' + sort[0].prop + ',' + sort[0].dir)
            .pipe(catchError(this.handleError));
    }

    agregar(ticket: Ticket): Observable<Ticket> {
        return this.http.post<Ticket>(this.url, ticket)
            .pipe(catchError(this.handleError));
    }

    modificar(ticket: Ticket): Observable<Ticket> {
        return this.http.put<Ticket>(this.url, ticket)
            .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error('El servidor ha retornado un error');
        }
        return throwError('Something bad happened; please try again later.');
    };
}
