import { Ticket } from './ticket.model';
import { Estado } from './estado.model';
import { Usuario } from './usuario.model';

export class TicketEstado {
  	codigo: number;
  	ticket: Ticket;
  	estado: Estado;
  	usuario: Usuario;
	comentario: string;
	fecha: Date;
	activo: number;
}