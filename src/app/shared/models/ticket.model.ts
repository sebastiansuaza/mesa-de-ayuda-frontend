import { Categoria } from './categoria.model';
import { Organizacion } from './organizacion.model';
import { TicketEstado } from './ticketEstado.model';
import { Usuario } from './usuario.model';

export class Ticket {
  	codigo: number;
  	descripcion: string;
  	persona: boolean;
	categoria: Categoria = new Categoria();
	organizacion: Organizacion = new Organizacion();
	usuario: Usuario = new Usuario();
	fecha: Date;
	estados: TicketEstado[];
}