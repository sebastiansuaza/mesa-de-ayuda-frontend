export class Organizacion {
  	codigo: number;
  	nombre: string;
	colorHex: string;
	totalTickets: number;
}