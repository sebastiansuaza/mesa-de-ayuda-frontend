import { Usuario } from './usuario.model';
import { Perfil } from './perfil.model';

export class UsuarioPerfil {
  	codigo: number;
  	usuario: Usuario;
	perfil: Perfil;
}