import { Persona } from './persona.model';
import { UsuarioOrganizacion } from './usuarioOrganizacion.model';
import { UsuarioPerfil } from './usuarioPerfil.model';

export class Usuario {
  	codigo: number;
  	persona: Persona = new Persona();
	organizacion: UsuarioOrganizacion = new UsuarioOrganizacion();
  	usuario: string;
	password: string;
	enabled: boolean = true;
	accountNonExpired: boolean = false;
	credentialNonExpired: boolean = false;
	accountNonLocked: boolean = false;
	perfiles: UsuarioPerfil[];
}

