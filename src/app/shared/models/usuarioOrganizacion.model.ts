import { Usuario } from './usuario.model';
import { Organizacion } from './organizacion.model';

export class UsuarioOrganizacion {
  	codigo: number;
  	usuario: Usuario;
	organizacion: Organizacion;
}