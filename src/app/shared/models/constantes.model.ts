import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  	providedIn: 'root'
})
export class Constantes {
    public static baseUrl: string = "http://localhost:8082/mesa_ayuda/api/";

    public static roleAdministrador: string = "ROLE_ADMINISTRADOR";
    public static roleUsuario: string = "ROLE_USUARIO";

    public static apiCategoria: string = "categoria";
    public static apiEstado: string = "estado";
    public static apiOrganizacion: string = "organizacion";
    public static apiPerfil: string = "perfil";
    public static apiTicket: string = "ticket";
    public static apiUsuario: string = "usuario";
    
    public static oopcionBoolean: any = [{
        estado: true,
        descripcion: "Si"
    },{
        estado: false,
        descripcion: "No"
    }];

    constructor() {
        
    };
}