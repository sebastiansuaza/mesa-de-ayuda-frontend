import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}
export interface Saperator {
  name: string;
  type?: string;
}
export interface SubChildren {
  state: string;
  name: string;
  type?: string;
}
export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
  child?: SubChildren[];
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  roles: string[];
  badge?: BadgeItem[];
  saperator?: Saperator[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: '',
    name: 'Inicio',
    type: 'saperator',
    icon: 'av_timer',
    roles: []
  }, {
    state: 'dashboards',
    name: 'Dashboard',
    type: 'link',
    icon: 'av_timer',
    roles: []
  }, {
    state: '',
    name: 'Administración',
    type: 'saperator',
    icon: 'av_timer',
    roles: ['ROLE_ADMINISTRADOR']
   }, {
    state: 'usuario',
    name: 'Gestion de usuarios',
    type: 'link',
    icon: 'perm_contact_calendar',
    roles: ['ROLE_ADMINISTRADOR']
  }, {
    state: '',
    name: 'Tickets',
    type: 'saperator',
    icon: 'av_timer',
    roles: ['ROLE_USUARIO']
  }, {
    state: 'ticket',
    name: 'Tickets',
    type: 'link',
    icon: 'av_timer',
    roles: ['ROLE_USUARIO']
  }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
