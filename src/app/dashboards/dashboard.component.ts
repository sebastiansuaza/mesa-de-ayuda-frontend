import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from 'ng-chartist';
declare var require: any;

import { Usuario } from '../shared/models/usuario.model';
import { Ticket } from '../shared/models/ticket.model'
import { Pagina } from '../shared/models/paged/pagina.model'
import { Organizacion } from '../shared/models/organizacion.model';

import { UsuarioService } from '../shared/services/usuario.service';
import { OrganizacionService } from '../shared/services/organizacion.service';
import { TicketService } from '../shared/services/ticket.service';
import { CategoriaService } from '../shared/services/categoria.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    usuario: Usuario;
    totalUsuarios: number = 0;
    totalCategorias: number = 0;
    totalOrganizaciones: number = 0;
    totalTickets: number = 0;
    titulo: string;
    perfil: string;

    pagina = new Pagina();
    loading = true;
    rows: Ticket[];
    sort: any[];
    tickets: Ticket[];
    organizaciones: Organizacion[];

    @ViewChild(DashboardComponent) table: DashboardComponent;
    constructor(private usuarioService: UsuarioService, private categoriaService: CategoriaService, 
      private organizacionService: OrganizacionService, private ticketService: TicketService) {
        this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
        for (var i = 0; i < this.usuario.perfiles.length; ++i) {
            if (this.usuario.perfiles[i].perfil.perfil === 'ROLE_ADMINISTRADOR') {
                this.perfil = 'ROLE_ADMINISTRADOR';
                this.titulo = "Ultimos tickets registrados";
                this.organizacionService.listar().subscribe(data => this.organizaciones = data);
            } else {
                this.perfil = 'ROLE_USUARIO';
                this.organizaciones.push(this.usuario.organizacion.organizacion);
                this.titulo = "Ultimos tickets registrados por: " + this.usuario.persona.nombre + ' ' + this.usuario.persona.apellido;
            }
        }

        this.usuarioService.total().subscribe(data => this.totalUsuarios = data);
        this.categoriaService.total().subscribe(data => this.totalCategorias = data);
        this.organizacionService.total().subscribe(data => this.totalOrganizaciones = data);
        this.ticketService.total().subscribe(data => this.totalTickets = data);
    }

    ngOnInit() {
        this.sort = [{ prop: "codigo", dir: "asc" }];
        this.setPage({ offset: 0 });
    }

    obtenerRegistros() {
        this.ticketService.obtenerPaginaPorUsuario(this.pagina.number, this.sort).subscribe(data => {
            this.pagina = data;
            this.rows = data.content;
            this.loading = false;
        });
    }

    setPage(pageInfo) {
        this.loading = true;
        this.pagina.number = pageInfo.offset;
        this.obtenerRegistros();
    }

    onSort(event) {
        var pagina = 0;
        this.sort[0].prop = event.sorts[0].prop;
        this.sort[0].dir = event.sorts[0].dir;

        this.ticketService.obtenerPaginaPorUsuario(pagina, this.sort).subscribe(data => {
            this.pagina = data;
            this.rows = data.content;
        });
    }
}
