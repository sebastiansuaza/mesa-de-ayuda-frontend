import { Routes } from '@angular/router';

import { AdministrarUsuariosComponent } from './administrar-usuarios/administrar-usuarios.component';
import { AgregarUsuariosComponent } from './agregar-usuarios/agregar-usuarios.component';

export const UsuariosRoutes: Routes = [{
	path: '',
	children: [{
    	path: '',
    	component: AdministrarUsuariosComponent
	}, {
		path: 'agregar',
		component: AgregarUsuariosComponent
	}]
}];
