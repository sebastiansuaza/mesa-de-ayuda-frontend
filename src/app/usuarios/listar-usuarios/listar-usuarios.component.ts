import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';

import { Usuario } from '../../shared/models/usuario.model';
import { Pagina } from '../../shared/models/paged/pagina.model';

import { UsuarioService } from '../../shared/services/usuario.service';

@Component({
	selector: 'app-listar-usuarios',
	templateUrl: './listar-usuarios.component.html',
	styleUrls: ['./listar-usuarios.component.css']
})
export class ListarUsuariosComponent implements OnInit {
	pagina = new Pagina();
	loading = true;
	rows: Usuario[];
	sort: any[];

	@ViewChild(ListarUsuariosComponent) table: ListarUsuariosComponent;
	constructor(private usuarioService: UsuarioService) {

	}

	ngOnInit() {
		this.sort = [{ prop: "codigo", dir: "asc" }];
		this.setPage({ offset: 0 });
	}

	obtenerRegistros() {
		this.usuarioService.obtenerPagina(this.pagina.number, this.sort).subscribe(data => {
			this.pagina = data;
			this.rows = data.content;
			this.loading = false;
		});
	}

	setPage(pageInfo) {
		this.loading = true;
		this.pagina.number = pageInfo.offset;
		this.obtenerRegistros();
	}

	onSort(event) {
		var pagina = 0;
		this.sort[0].prop = event.sorts[0].prop;
		this.sort[0].dir = event.sorts[0].dir;

		this.usuarioService.obtenerPagina(pagina, this.sort).subscribe(data => {
			this.pagina = data;
			this.rows = data.content;
		});
	}
}
