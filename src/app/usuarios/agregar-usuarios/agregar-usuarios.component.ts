import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { Persona } from '../../shared/models/persona.model';
import { Perfil } from '../../shared/models/perfil.model';
import { UsuarioPerfil } from '../../shared/models/usuarioPerfil.model';
import { Organizacion } from '../../shared/models/organizacion.model';
import { UsuarioOrganizacion } from '../../shared/models/usuarioOrganizacion.model';
import { Usuario } from '../../shared/models/usuario.model';

import { PerfilService } from '../../shared/services/perfil.service';
import { OrganizacionService } from '../../shared/services/organizacion.service';
import { UsuarioService } from '../../shared/services/usuario.service';

@Component({
  selector: 'app-agregar-usuarios',
  templateUrl: './agregar-usuarios.component.html',
  styleUrls: ['./agregar-usuarios.component.css']
})
export class AgregarUsuariosComponent implements OnInit {
	submitted = false;
	viewOrganizaciones = false;

	usuario: Usuario;
	perfil: Perfil;
	perfiles: Perfil[];
	organizacion: Organizacion;
	organizaciones: Organizacion[];

	constructor(private location: Location, private router: Router, private perfilService: PerfilService, 
		private organizacionService: OrganizacionService, private usuarioService: UsuarioService) {
		this.usuario = new Usuario();
		this.perfil = new Perfil();
		this.organizacion = new Organizacion();
  	}

  	ngOnInit() {
		this.usuario.perfiles = [];

		this.perfilService.listar().subscribe(data => {
				this.perfiles = data;
			}, error => console.log(error)
		)
  	}

	seleccionarPerfil(event) {
		var perfil: UsuarioPerfil = new UsuarioPerfil();
		var perfiles: UsuarioPerfil[] = [];
		perfil.perfil = event.value;
		perfiles.push(perfil);

		if (event.value != undefined) {
			if (event.value.codigo == 1) {
				this.viewOrganizaciones = false;
				this.usuario.perfiles = perfiles;
				this.usuario.organizacion = null;
			} else {
				this.viewOrganizaciones = true;
				this.usuario.perfiles = perfiles;
				this.organizacionService.listar().subscribe(data => {
						this.organizaciones = data;
					}, error => console.log(error)
				)
			}
		}
	}

	seleccionarOrganizacion(event) {
		if (event.value != undefined) {
			var organizacion: UsuarioOrganizacion = new UsuarioOrganizacion();
			organizacion.organizacion = event.value;
			this.usuario.organizacion = organizacion;
		}
	}

	volver() {
		this.location.back();
	}

	onSubmit() {
		this.submitted = true;
		this.usuarioService.agregar(this.usuario).subscribe(
			data => {
				//this.notificacionService.notificar("Agregado con exito");
				this.router.navigate(['/usuario']);
			}, error => { 
				this.submitted = false;
				console.log(error)
			}
		)
	}
}
