import 'hammerjs';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { UsuariosRoutes } from './usuarios.routing';

import { ListarUsuariosComponent } from './listar-usuarios/listar-usuarios.component';
import { AgregarUsuariosComponent } from './agregar-usuarios/agregar-usuarios.component';
import { AdministrarUsuariosComponent } from './administrar-usuarios/administrar-usuarios.component';

@NgModule({
	imports: [
		CommonModule,
		DemoMaterialModule,
		FormsModule,
		ReactiveFormsModule,
		FlexLayoutModule,
		NgxDatatableModule,
		RouterModule.forChild(UsuariosRoutes)
	],
  	declarations: [ListarUsuariosComponent, AgregarUsuariosComponent, AdministrarUsuariosComponent]
})
export class UsuariosModule { }
